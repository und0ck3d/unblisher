package core

import (
	"bytes"
	"regexp"
	"strings"

	"github.com/jung-kurt/gofpdf"
)

type DocumentImage struct {
	Name    string
	Content []byte
	URL     string
}

func (img *DocumentImage) Download() error {
	content, err := httpGet(img.URL)
	if err != nil {
		return err
	}

	img.Content = content

	return nil
}

type Document struct {
	Images     []*DocumentImage
	OutputPath string

	url string
}

func NewDocument(url, outputPath string) (*Document, error) {
	doc := &Document{OutputPath: outputPath, url: url}

	if err := doc.FetchInfo(); err != nil {
		return nil, err
	}

	return doc, nil
}

// FetchInfo downloads the page and tries
// to find the images' names and base URL.
func (doc *Document) FetchInfo() error {
	raw, err := httpGet(doc.url)
	if err != nil {
		return err
	}

	imagesBaseURL := findImagesBaseURL(string(raw))
	imagesNames := findImagesNames(string(raw))

	for _, imgName := range imagesNames {
		img := &DocumentImage{
			Name: imgName,
			URL:  imagesBaseURL + imgName,
		}

		doc.Images = append(doc.Images, img)
	}

	return nil
}

// GeneratePDF will create a new PDF with a
// page for each image in `.Images`. The final PDF is written
// to `outputPath`.
func (doc *Document) GeneratePDF() error {
	// Init PDF
	pdf := gofpdf.New("P", "mm", "A4", "")

	// Add pages with images
	for _, img := range doc.Images {
		pagew, pageh := pdf.GetPageSize()

		pdf.AddPage()
		pdf.RegisterImageReader(img.Name, "jpg", bytes.NewReader(img.Content))
		pdf.Image(img.Name, 0, 0, pagew, pageh, false, "", 0, "")
	}

	// Write file and close
	return pdf.OutputFileAndClose(doc.OutputPath)
}

func findImagesBaseURL(raw string) string {
	re := regexp.MustCompile("imagePath = \"(.|\n)*?\"")
	url := re.FindAllString(raw, -1)[0]
	url = strings.Replace(url, "imagePath = \"", "", -1)
	url = strings.Replace(url, "\"", "", -1)

	return url
}

func findImagesNames(raw string) []string {
	re := regexp.MustCompile("file: '(.|\n)*?'")
	result := re.FindAllString(raw, -1)
	images := []string{}

	for _, f := range result {
		f = strings.Replace(f, "file: '", "", -1)
		f = strings.Replace(f, "'", "", -1)

		images = append(images, f)
	}

	return images
}
