package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/und0ck3d/unblisher/core"
)

var unblisherCmd = &cobra.Command{
	Version: "0.0.1",
	Use:     "unblisher",
	Short:   "A tool for downloading documents from Youblisher in PDF format.",
	Args:    cobra.MinimumNArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		log.Println("Initializing document...")
		doc, err := core.NewDocument(args[0], args[1])
		if err != nil {
			log.Panicf("error initializing document: %v", err)
		}

		log.Printf("Downloading images (total: %d)...", len(doc.Images))
		for i, img := range doc.Images {
			log.Printf("Downloading image %d of %d: %s", i+1, len(doc.Images), img.Name)
			if err = img.Download(); err != nil {
				log.Panicf("error downloading image %s: %v", img.Name, err)
			}
		}

		log.Printf("Generating PDF to %s...", doc.OutputPath)
		if err = doc.GeneratePDF(); err != nil {
			log.Panicf("error generating PDF: %v", err)
		}
	},
}

func Execute() {
	if err := unblisherCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
