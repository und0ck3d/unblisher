# Unblisher

## Download

```bash
$ go get -u https://gitlab.com/und0ck3d/unblisher
```

## Building

```bash
$ go build -o ./bin/unblisher main.go
```

## Usage

```bash
$ ./bin/unblisher [document_url] [output_file]
```